<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('chat', ['middleware' => ['cors'], 'uses' => 'ChatController@index']);
$router->post('chat', ['middleware' => ['cors'], 'uses' => 'ChatController@chat']);
$router->get('time', ['middleware' => ['cors'], 'uses' => 'ChatController@nowTime']);
$router->post('test', ['middleware' => ['cors'], 'uses' => 'ChatController@test']);

$router->post('chat/store', ['middleware' => ['cors'], 'uses' => 'ChatController@store']);
$router->post('chat/apply', ['middleware' => ['cors'], 'uses' => 'ChatController@apply']);
$router->post('chat/quota', ['middleware' => ['cors'], 'uses' => 'ChatController@quota']);

$router->post('/login', 'UserController@login');
$router->post('/register', 'UserController@register');

$router->get('/user/{id}', ['middleware' => 'auth', 'uses' =>  'UserController@get_user']);

$router->get('/', function () use ($router) {
    return $router->app->version();
});
