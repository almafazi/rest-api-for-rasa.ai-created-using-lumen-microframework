<?php

namespace App\Http\Controllers;

use App\User;
use App\Message;
use App\Quota;
use App\Furlough;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ChatController extends Controller
{
    private 
        $response, 
        $status = 200, 
        $client, 
        $rasa_url,
        $rasa_ip;

    public function __construct()
    {
        session_start();
        $this->client = new \GuzzleHttp\Client();
        $this->rasa_url = 'http://localhost:5005/webhooks/rest/webhook';
        $this->rasa_ip = 'localhost';
    }
    
    public function quota(Request $request) {
        $sender = $request->input('user');
        $type  = $request->input('type');
    }
    public function test(Request $request) {
         return response()->json(['msg' => $request->input('msg')]);
    }
    
    public function nowTime(Request $request) {
        date_default_timezone_set('Asia/Jakarta');
        
        $time = date("H");
        $timezone = date("e");
        if ($time < "12") {
           $nowTime = "pagi";
        } else
        if ($time >= "12" && $time < "15") {
            $nowTime = "siang";
        } else
        if ($time >= "15" && $time < "18") {
            $nowTime = "sore";
        } else
        if ($time >= "18") {
            $nowTime = "malam";
        }
    
        return response()->json(['time' => $nowTime]);
    }
    
    public function index()
    {
        app('session')->put('tes', 'Welcome to Lumen REST API Framework For Rasa.AI Backend!');
        echo app('session')->get('tes');
    }
    
    public function chat(Request $request) {
        
        if(empty($_SESSION['sender'])) {
            
            $_SESSION['sender'] = 0;
            
            $sender = 0;
            
        } else {
            
            $sender = $_SESSION['sender'];
            
        }
        
        if($request->input('query') == 'clear') {
            
            session_destroy();
            
            $sender = $_SESSION['sender'] = 0;
            
        $quota = Quota::where('user_id', $user_id)->whereType($type)->pluck('quota');
                    
        return response()->json(['msg' => $quota]);
    }
    }

    
    public function apply(Request $request) {
        $additional      = convertToDay($request->input('days'));
        $user_id         = $request->input('user');
        
        $start_date      = date('Y-m-d', strtotime($this->tanggalIndo($request->input('start_date'))));
        $finish_date     = date('Y-m-d', strtotime($this->tanggalIndo($request->input('start_date')).' + '.$additional));
        
        $total_days      = $additional;
        $additional_info = $request->input('cuti_tipe');
                    
        Furlough::create(compact('user_id', 'start_date', 'finish_date', 'total_days', 'additional_info'));
    }
    
    public function index()
    {
        echo 'It\'s Work!';
    }

    /* ADDITIONAL FUNCTION */
    public function tanggalIndo($tanggal) {
        
        $tgl = array (
            'januari'   => 'january',
            'februari'  => 'february',
            'maret'     => 'march',
            'april'     => 'april',
            'mei'       => 'may',
            'juni'      => 'june',
            'juli'      => 'july',
            'agustus'   => 'august',
            'september' => 'september',
            'oktober'   => 'october',
            'november'  => 'november',
            'desember'  => 'december'
        );

        return strtr(strtolower($tanggal), $tgl);
    }
    
    public function convertToDay($data) {
        
        $tgl = array (
            'hari'      => 'day',
            'hr'        => 'day',
            'bulan'     => 'month',
            'bln'       => 'month',
            'bl'        => 'month',
        );

        return strtr(strtolower($data), $tgl);
    }
    
    public function calculateDays($start_Date, $finish_Date) {
        $startTimeStamp = strtotime($start_date);
        $endTimeStamp = strtotime($finish_date);

        $timeDiff = abs($endTimeStamp - $startTimeStamp);
        $numberDays = $timeDiff/86400;
        return intval($numberDays);
    }

}
